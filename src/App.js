import './App.css';
import Book from './components/book.jsx'

function App() {
  return (
    <div className="App">
      <Book/>
    </div>
  );
}

export default App;
