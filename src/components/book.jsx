import { useRef, useState, useEffect } from 'react';
import HTMLFlipBook from 'react-pageflip';
import { pdfjs } from 'react-pdf';
import { Document, Page } from 'react-pdf';
pdfjs.GlobalWorkerOptions.workerSrc = '/js/pdf.worker.min.js';


const Book = () => {
    const flip = useRef();
    const [currentPage, setCurrentPage] = useState(1);
    const [pageNumber, setPageNumber] = useState(null);

    const onFlip = e => {
        console.log('current page: ', e.data + 1);
        setTimeout(() => setCurrentPage(e.data + 1), 1100); // ждем пока анимация закончится
    }

    const onDocumentLoadSuccess = ({ numPages }) => {
        if (pageNumber === null) {
            console.log('page number:', numPages);
            setPageNumber(numPages);
        }
    }

    const [blob, setBlob] = useState(null);

    useEffect(() => {
        fetch('https://arxiv.org/pdf/quant-ph/0410100.pdf')
        .then(res => res.blob()) // Gets the response and returns it as a blob
        .then(blob => {
            const reader = new FileReader();
            reader.readAsDataURL(blob); 
            reader.onloadend = function() {
                const base64data = reader.result;
                setBlob(base64data);
            }
        });
    }, []);

    return (
        <Document file={blob} onLoadSuccess={onDocumentLoadSuccess}>
            <HTMLFlipBook width={600} height={900} ref={flip} onFlip={onFlip}>
                {Array(pageNumber || 6).fill(1).map((el, i) => 
                    <div className="page-wrapper" key={`page-${i}`}>
                        {Math.abs(currentPage - i) < 6 || true ? <Page pageIndex={i}/> : <div></div>}
                    </div>
                )}
            </HTMLFlipBook>
        </Document>
    )
}

export default Book;